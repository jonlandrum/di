Don't bug me, Digitally Imported
================================

This is a script for Greasemonkey/Tampermonkey/Violentmonkey/etc that auto-closes the "Are you still there?" dialog box that pops up every 30 minutes on [di.fm](http://www.di.fm/ "Digitally Imported"). This script is necessary because the popup appears even if the user is logged in, and even if the user is a Premium subscriber.

Usage
-----

* Install [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/ "Add-ons for Firefox")/[Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo "Chrome Web Store")/[Violentmonkey](https://addons.opera.com/en/extensions/details/violent-monkey/?display=en)
* [Add this script as a user script](https://github.com/jonlandrum/di/raw/master/di.user.js "Install di.user.js")

Limitations and Known Issues
----------------------------

The way Digitally Imported present their "Are you still there?" dialog box is the same way they present their login dialog box. Thus, if the script is actively running, you will need to login from their [dedicated login page](https://www.di.fm/login). Attempting to log in from any other page on the site will result in frustration, as the script will auto-close that dialog, too.


Caveat
------

Because this script allows you to keep the music playing without any interraction with the site, it is up to you to conscientiously pause the music when you leave the room to keep from wasting Digitally Imported's resources (their internet bill is probably huge already).

Dissemination
-------------

This project is also listed at [Greasy Fork](https://greasyfork.org/en/scripts/19792-don-t-bug-me-digitally-imported).
